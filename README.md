# MongoDB template for BastilleBSD

Template for [BastilleBSD](https://bastillebsd.org/) to run a
[MongoDB](https://www.mongodb.com/) database inside of a
[FreeBSD](https://www.freebsd.org/) jail.

By default the hard coded version of MongoDB (see
[Bastillefile](Bastillefile)) will be installed and the service will be
configured to allow network access.

Furthermore a default superuser will be created!

The version and the credentials for the superuser can be set via arguments.

## License

This program is distributed under 3-Clause BSD license. See the file
[LICENSE](LICENSE) for details.

## Bootstrap

So far bastille only supports downloading from GitHub or GitLab, so you have
to fetch the template manually:

```
# mkdir <your-bastille-template-dir>/wegtam
# git -C <your-bastille-template-dir>/wegtam clone https://codeberg.org/wegtam/bastille-mongodb.git
```

## Usage

### 1. Install the default version into a jail

```
# bastille template TARGET wegtam/bastille-mongodb
```

### 2. Install with custom settings

```
# bastille template TARGET wegtam/bastille-mongodb --arg VERSION=49 --arg USER=perfuma --arg PASSWORD=supersecret
```

